import logging as log
from unittest import TestCase

from lab import lab61 as lab

log.basicConfig(level=log.DEBUG)


class TestLab61(TestCase):
    def test123(it):
        it.assertEqual(0, lab.count_holes('123'))

    def test906(it):
        it.assertEqual(3, lab.count_holes('906'))

    def testShouldBeDecimalFirst(it):
        it.assertEqual(0, lab.count_holes('001'))

    def testMinusEight(it):
        it.assertEqual(2, lab.count_holes('-8'))

    def testMinusFlatEight(it):
        it.assertEqual('ERROR', lab.count_holes('-8.0'))
