from unittest import TestCase

from lab import lab51


class TestLab51(TestCase):
    def testCleanSameDigits(self):
        result = lab51.clean_list([1, 1.0, '1', -1, 1])
        test = [1, 1.0, '1', -1]
        self.assertListEqual(test, result)

    def testCleanSameString(self):
        result = lab51.clean_list(['qwe', 'reg', 'qwe', 'REG'])
        test = ['qwe', 'reg', 'REG']
        self.assertListEqual(test, result)

    def testFloatNumbers(self):
        result = lab51.clean_list([32, 32.1, 32.0, -123])
        test = [32, 32.1, 32.0, -123]
        self.assertListEqual(test, result)

    def testNumbersAndString(self):
        result = lab51.clean_list([1, 2, 1, 1, 3, 4, 5, 4, 6, '2', 7, 8, 9, 0, 1, 2, 3, 4, 5])
        test = [1, 2, 3, 4, 5, 6, '2', 7, 8, 9, 0]
        self.assertListEqual(test, result)
