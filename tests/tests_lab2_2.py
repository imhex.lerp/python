import unittest

from lab import lab2_2

testcase1 = "Everybody sing a song: la-la la-la la-la!"
testcase2 = "Everybody sing a song: ."


class TestLab22(unittest.TestCase):
    def test_2_3_1(self):
        self.assertEqual(lab2_2.lab2_2(2, 3, 1), testcase1)

    def test_1_0_0(self):
        self.assertEqual(lab2_2.lab2_2(1, 0, 0), testcase2)
