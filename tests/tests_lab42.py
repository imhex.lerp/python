import logging
import unittest

from lab import lab42

logging.basicConfig(level=logging.DEBUG)


class TestLab42(unittest.TestCase):

    def test1(self):
        self.assertEqual(lab42.lab42(['1']), '1')

    def test123(self):
        self.assertEqual(lab42.lab42('qwe asd zxc 123'.split(" ")), '123 zxc asd qwe')

    def testPadavan(self):
        self.assertEqual(lab42.lab42('padawan young my HAVE MUST YOU PATIENCE'.split(" ")), 'PATIENCE YOU MUST HAVE my young padawan')
