import logging as log
from unittest import TestCase

from lab import lab54 as lab

log.basicConfig(level=log.DEBUG)


class TestLab53(TestCase):
    def testIdeas(self):
        result = lab.file_search(
            ['C:',
             'backup.log',
             'ideas.txt'
             ]

            , 'ideas.txt')
        self.assertEqual('C:/ideas.txt', result)

    def testFalse(self):
        result = lab.file_search(
            ['D:',
             ['recycle bin'],
             ['tmp',
              ['old'],
              ['new folder1',
               'asd.txt',
               'asd.bak',
               'find.me.bak']
              ],
             'hey.py'
             ]

            , 'find.me')

        # D:/recycle bin
        # D:/tmp/old
        # D:/tmp/hey.py
        # D:/tmp/new folder1/asd.txt
        # D:/tmp/new folder1/asd.bak
        # D:/tmp/new folder1/find.me.bak

        self.assertEqual(False, result)

    def testHereIam(self):
        result = lab.file_search(
            ['/home',
             ['user1'],
             ['user2',
              ['my pictures'],
              ['desktop',
               'not this',
               'and not this',
               ['new folder',
                'hereiam.py']]],
             'work.ovpn',
             'prometheus.7z',
             ['user3',
              ['temp'],
              ],
             'hey.py']
            , 'hereiam.py')
        self.assertEqual('/home/user2/desktop/new folder/hereiam.py', result)
