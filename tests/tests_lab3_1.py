import unittest

from lab import lab3_1

wrong = "not triangle"
correct = "triangle"


class TestLab31(unittest.TestCase):
    def test_10_20_30(self):
        self.assertEqual(lab3_1.lab3_1(10, 20, 30), wrong)

    def test_1_1_1(self):
        self.assertEqual(lab3_1.lab3_1(1, 1, 1), correct)

    def test_55_55_minus2(self):
        self.assertEqual(lab3_1.lab3_1(5.5, 5.5, -2), wrong)
