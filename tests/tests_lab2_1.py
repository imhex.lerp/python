import unittest

from lab import lab2_1


class Test(unittest.TestCase):
    def test_1_1_025(self):
        self.assertEqual(lab2_1.lab2_1(1, 1, 0.25), 1.5957691216057308)
