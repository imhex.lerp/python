import logging as log
from string import ascii_uppercase as alphabet
from unittest import TestCase
from lab import lab62 as lab

log.basicConfig(level=log.DEBUG)

morse_code = {}


class TestLab62(TestCase):
    def testSOS(it):
        it.assertEqual(
            '^_^_^___^^^_^^^_^^^___^_^_^',
            lab.encode_morze('sos'))

    def testMorzeCode(it):
        it.assertEqual(
            '^^^_^^^___^^^_^^^_^^^___^_^^^_^___^^^_^^^_^_^___^_______^^^_^_^^^_^___^^^_^^^_^^^___^^^_^_^___^',
            lab.encode_morze('Morze code'))

    def testPrometheus(it):
        it.assertEqual('^_^^^_^^^_^___^_^^^_^___^^^_^^^_^^^___^^^_^^^___^___^^^___^_^_^_^___^___^_^_^^^___^_^_^',
                       lab.encode_morze('Prometheus'))

    def testMorse_codeDictionaryIsAlphabet(it):
        exec('global morse_code; ' + lab.encode_morze('1'))
        it.assertEqual(len(alphabet), len(morse_code))
        it.assertListEqual(list(alphabet), list(morse_code.keys()))
