import logging
import unittest

from lab import lab41

logging.basicConfig(level=logging.DEBUG)


class TestLab41(unittest.TestCase):
    def test0(self):
        self.assertEqual(lab41.lab41('0'), 'YES')

    def test_puppy(self):
        self.assertEqual(lab41.lab41("muppy"), "NO")

    def test_some_text(self):
        self.assertEqual(lab41.lab41("mystring1Gni rts ym"), "YES")
