import logging as log
from unittest import TestCase
from lab import lab63 as lab

log.basicConfig(level=log.DEBUG)


class TestLab62(TestCase):
    def testNoPointDueToRow(it):
        it.assertFalse(lab.saddle_point([[1, 1, 1], [3, 2, 1]]))

    def testNoPointDueToColumn(it):
        it.assertFalse(lab.saddle_point([[1, 2, 3], [3, 2, 1]]))

    def testThereArePoint(it):
        it.assertEqual(
            (1, 2),
            lab.saddle_point([
                [8,3,0,1,2,3,4,8,1,2,3],
                [3,2,1,2,3,9,4,7,9,2,3],
                [7,6,0,1,3,5,2,3,4,1,1]]))

    def testPointOnDigit(it):
        it.assertEqual(
            (0, 0),
            lab.saddle_point([[21]]))
