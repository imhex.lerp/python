import logging
import unittest

from lab import lab44

logging.basicConfig(level=logging.DEBUG)


class TestLab44(unittest.TestCase):
    def testShouldThrowValueErrorWhenLessThanZero(self):
        with self.assertRaises(Exception):
            lab44.lab44([-1, 1])

    def testShouldThrowValueErrorWhenLargerThanMaxValue(self):
        with self.assertRaises(Exception):
            lab44.lab44([1000000, 1000001])

    def test_0_1000(self):
        self.assertEqual(1, lab44.lab44([0, 1000]))

    def test_1001_1122(self):
        self.assertEqual(3, lab44.lab44([1001, 1122]))

    def test_222222_222333(self):
        self.assertEqual(7, lab44.lab44([222222,222333]))


if __name__ == '__main__':
    unittest.main()
