import logging
import unittest

from lab import lab43

logging.basicConfig(level=logging.DEBUG)


class TestsLab43(unittest.TestCase):
    def testUnclosedBraces(self):
        self.assertEqual(lab43.lab43(')('), 'NO')

    def testTwoUnclosedBraceSequence(self):
        self.assertEqual(lab43.lab43('(()(()'), 'NO')

    def testClosedBraceSequence(self):
        self.assertEqual(lab43.lab43('(()(()()))'), 'YES')

    def testUnclosedBraceSequence(self):
        self.assertEqual(lab43.lab43('())()(()())(())'), 'NO')
