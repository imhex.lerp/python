import unittest

from lab import lab3_2


class TestLab32(unittest.TestCase):
    def test_0(self):
        self.assertEqual(lab3_2.lab3_2(0), 0, "fibonacci number of 0 to be 0")

    def test_1(self):
        self.assertEqual(lab3_2.lab3_2(1), 1)

    def test_2(self):
        self.assertEqual(lab3_2.lab3_2(2), 1)

    def test_3(self):
        self.assertEqual(lab3_2.lab3_2(3), 2)

    def test_4(self):
        self.assertEqual(lab3_2.lab3_2(4), 3)

    def test_5(self):
        self.assertEqual(lab3_2.lab3_2(5), 5)

    def test_6(self):
        self.assertEqual(lab3_2.lab3_2(6), 8)

    def test_7(self):
        self.assertEqual(lab3_2.lab3_2(7), 13)

    def test_8(self):
        self.assertEqual(lab3_2.lab3_2(8), 21)

    def test_9(self):
        self.assertEqual(lab3_2.lab3_2(9), 34)

    def test_10(self):
        self.assertEqual(lab3_2.lab3_2(10), 55)

    def test_11(self):
        self.assertEqual(lab3_2.lab3_2(11), 89)

    def test_20(self):
        self.assertEqual(lab3_2.lab3_2(20), 6765)
