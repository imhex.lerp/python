from unittest import TestCase
import logging as log
from lab import lab53 as lab

log.basicConfig(level=log.DEBUG)


class TestLab53(TestCase):
    def testCase1(self):
        self.assertEqual(1, lab.super_fibonacci(2, 1))

    def testCase2(self):
        self.assertEqual(1, lab.super_fibonacci(3, 5))

    def testCase3(self):
        self.assertEqual(21, lab.super_fibonacci(8, 2))

    def testCase4(self):
        self.assertEqual(57, lab.super_fibonacci(9, 3))
