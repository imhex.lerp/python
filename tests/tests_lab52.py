from unittest import TestCase
import logging as log
from lab import lab52

log.basicConfig(level=log.DEBUG)


class TestLab52(TestCase):
    def testCase1(self):
        self.assertEqual(1, lab52.counter(12345, 567))

    def testCase2(self):
        self.assertEqual(2, lab52.counter(1233211, 12128))

    def testCase3(self):
        self.assertEqual(0, lab52.counter(123, 45))
