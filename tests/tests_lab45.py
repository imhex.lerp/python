import unittest

from lab import lab45


source1 = 'Prometheus'
encoded1 = 'wELcOMe To The HOtEL caLiFORNIa SUcH A LoVeLY PLaCE sucH a LOvely'

source2 = 'wiki'
encoded2 = 'I canT DAnCE i CANt TAlK Hey'

source3 = 'wewillrockyou'
encoded3 = 'Hot sUn BEATIng dOWN bURNINg mY FEet JuSt WalKIng arOUnD HOt suN mAkiNG me SWeat'

class TestLab45(unittest.TestCase):
    def testEncodingPrometheus(self):
        self.assertEqual(source1, lab45.decode([encoded1]))

    def testEncodingWiki(self):
        self.assertEqual(source2, lab45.decode([encoded2]))

    def testEncodingWewillrockyou(self):
        self.assertEqual(source3, lab45.decode([encoded3]))
