import math
import sys


def lab2_1(ex, mu, sigma):
    a = 1 / (sigma * math.sqrt(2.0 * math.pi))
    b = math.pow(ex - mu, 2)
    c = 2 * math.pow(sigma, 2)
    return a * math.exp(-(b / c))


def main(argv):
    try:
        chi, mu, sigma = argv
    except ValueError:
        print("usage: python lab2_1.py <chi> <mu> <sigma>")
        sys.exit(2)

    result = lab2_1(float(chi), float(mu), float(sigma))
    print(result)


if __name__ == '__main__':
    main(sys.argv[1:])
