import sys
import logging as log


def super_fibonacci(n, m):
    fi = [1 for i in range(m)]
    log.debug("initial = " + str(fi))
    if n > m:
        for i in range(n - m):
            m_ = fi[len(fi) - m::]
            sum1 = sum(m_)
            log.debug("next is %s which sum of %s", str(sum1), m_)
            fi.append(sum1)
    log.debug(fi)
    return fi[n - 1]


def main(param):
    try:
        a, b = param
        a = int(a)
        if a <= 0:
            raise ValueError
        if b > 35:
            raise ValueError
        b = int(b)
    except ValueError:
        print("lab53.py calculate super_fibonacci [n] element of [1..m] fiboncacci sequence")
        print(", where F(m) = 1 and next is sum of last 'm' elements")
        print("usage: python lab53.py <digit>, <digit>")
        sys.exit(2)

    print("a = ", a)
    print("b = ", b)
    print("result = ", super_fibonacci(a, b))


if __name__ == '__main__':
    main(sys.argv[1:])
