import sys

prefix = "Everybody sing a song: "
versus = "la"
dot = "."
exclamation = "!"


def lab2_2(x, y, z):
    song = ((((versus + "-") * x)[:-1] + " ") * y)[:-1]
    ending = exclamation if z == 1 else dot
    return prefix + song + ending


def main(argv):
    try:
        x, y, z = argv
        x = int(x)
        y = int(y)
        z = int(z)
        if x < 1:
            raise ValueError
        if y < 0:
            raise ValueError
        if z < 0 | z > 1:
            raise ValueError
    except ValueError:
        print("usage: python lab2_2.py <number[1..n]> <number[0..n]> <number[0,1]>")
        sys.exit(2)

    result = lab2_2(x, y, z)
    print(result)


if __name__ == '__main__':
    main(sys.argv[1:])
