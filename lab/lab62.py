import logging as log
import pprint
import sys

console_mode = False
morse_code = {
    "A": ".-",
    "B": "-...",
    "C": "-.-.",
    "D": "-..",
    "E": ".",
    "F": "..-.",
    "G": "--.",
    "H": "....",
    "I": "..",
    "J": ".---",
    "K": "-.-",
    "L": ".-..",
    "M": "--",
    "N": "-.",
    "O": "---",
    "P": ".--.",
    "Q": "--.-",
    "R": ".-.",
    "S": "...",
    "T": "-",
    "U": "..-",
    "V": "...-",
    "W": ".--",
    "X": "-..-",
    "Y": "-.--",
    "Z": "--.."
}
special = {' ': ' '}
signal = {
    ".": "^",
    "-": "^^^",
    " ": "_",
    "1": "_",
    "3": "_" * 3
}


def to_morse(input):
    return [list({**morse_code, **special}[char.upper()]) for char in str(input)]


def to_signal(sentence):
    words = [interleave(word, '1') for word in interleave(sentence, '3')]
    log.debug('words %s', words)
    return ''.join([signal[letter] for word in words for letter in word])


def interleave(arr, item):
    phrases = [item] * (len(arr) * 2 - 1)
    phrases[0::2] = arr
    return phrases


def encode_morze(input):
    if input == '1':
        return 'morse_code = ' + pprint.pformat(indent=4, object=morse_code)

    translated = to_morse(input)
    log.debug('input <%s> translated to %s', input, translated)

    encoded = to_signal(translated)
    log.debug('input <%s> encoded to %s', input, encoded)

    return encoded


def console(output):
    if console_mode:
        print(output)
    log.debug(output)


if __name__ == '__main__':
    console_mode = True
    encode_morze(sys.argv[1])
    pass
