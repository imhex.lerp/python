import sys

import logging as log

def counter(a, b):
    aarr = [v for v in str(a)]
    log.debug("a = "+ str(aarr))
    barr = [v for v in str(b)]
    log.debug("b = "+ str(barr))
    intersection = list(set(aarr) & set(barr))
    log.debug("intersection = "+ str(intersection))
    return len(intersection)


def main(param):
    try:
        a, b = param
        a = int(a)
        b = int(b)
    except ValueError:
        print("lab52.py shows how many numbers of second parameter contains in first parameter")
        print("usage: python lab52.py <digit>, <digit>")
        sys.exit(2)

    print("a = ", a)
    print("b = ", b)
    print("result = ", counter(a, b))

if __name__ == '__main__':
    main(sys.argv[1:])
