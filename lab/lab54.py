import sys
import logging as log

class Node:
    def __init__(self, o):
        self.filename: str = o
        self.parent: Node = None
        self.children: list = list()


def arr2tree(parent, value):

    if isinstance(value, list):
        node = Node(value[0])
        for i in value[1::]:
            arr2tree(node, i)
    else:
        log.debug("data:%s", value)
        node = Node(value)

    node.parent = parent
    parent.children.append(node)

    log.debug("append node: " +str(parent.filename)+"->"+ str(node.filename))

    return node


def file_search(arr, searchCriteria):
    root = arr2tree(Node(None), arr)
    print(root.filename)
    print(len(root.children))
    print(root.children)

log.basicConfig(level=log.DEBUG)
file_search(['c:', 'file1.txt', 'file2.txt'], '')


def main(param):
    try:
        a, b = param
    except ValueError:
        print("lab54.py ")
        print("usage: python lab54.py <folder list>, <filename>")
        sys.exit(2)

    print("a = ", a)
    print("b = ", b)
    print("result = ", file_search(a, b))


if __name__ == '__main__':
    # main(sys.argv[1:])
    pass
