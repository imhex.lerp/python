import sys

correct = "triangle"
wrong = "not triangle"


def lab3_1(a, b, c):
    if (a <= 0.0) | (b <= 0.0) | (c <= 0.0):
        return wrong

    li = [a, b, c]
    li.sort()
    a, b, c = li
    if a + b > c:
        return correct
    else:
        return wrong


def main(argv):
    try:
        a, b, c = argv
        a = float(a)
        b = float(b)
        c = float(c)
    except ValueError:
        print("usage: python lab3_1.py <float> <float> <float>")
        print("       Example: python lab3_1.py 1.5 2.3 3.5")
        sys.exit(2)

    print("Result: " + lab3_1(a, b, c))


if __name__ == '__main__':
    main(sys.argv[1:])
