import sys


def fibonacci():
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a + b


def lab3_2(number):
    if number == 0:
        return 0

    cur = 0
    for i in fibonacci():
        if cur < number:
            cur += 1
        else:
            return i


def main(argv):
    try:
        a = int(argv)
        if a < 0:
            raise ValueError
    except ValueError:
        print("usage: python lab3_2.py <number>")
        sys.exit(2)

    print("Fibonacci number of %d = %d", a, lab3_2(a))


if __name__ == '__main__':
    main(sys.argv[1])
