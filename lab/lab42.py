import logging
import sys


def lab42(argv):
    reversedArraySpaceSeparated = " ".join(argv[::-1])
    logging.debug(reversedArraySpaceSeparated)
    return reversedArraySpaceSeparated


if __name__ == '__main__':
    lab42(sys.argv[1:])
