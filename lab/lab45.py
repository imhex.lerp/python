import sys

key = 'aaaaabbbbbabbbaabbababbaaababaab'

shift = ord('a')
dictionary = [key[i - shift:i + 5 - shift] for i in range(shift, ord('z'))]  # translate a-z

def decodeSymbol(symbol):
    if symbol.lower() == symbol:
        return 'a'
    else:
        return 'b'

def decode(param):
    nospaces = param[0].replace(' ', '')

    matched = (decodeSymbol(nospaces[i]) for i in range(0, len(nospaces)))

    print(matched)

    pass


sys.argv = [0, "wELcOMe To The HOtEL caLiFORNIa SUcH A LoVeLY PLaCE sucH a LOvely"]

if __name__ == '__main__':
    decode(sys.argv[1:])
