import logging
import sys


def lab43(braces):
    logging.debug("input: %s", braces)

    opened_braces = 0
    for brace in "".join(braces):
        if brace == ")":
            opened_braces -= 1
        elif brace == "(":
            opened_braces += 1
        else:
            raise ValueError("symbol {0} is not brace".format(brace))

        if opened_braces < 0:
            return "NO"

    if opened_braces == 0:
        return "YES"
    return "NO"


if __name__ == '__main__':
    lab43(sys.argv[1:])
