import logging
import sys


def validate(argv):
    try:
        a1 = int(argv[0])
        a2 = int(argv[1])
    except ValueError as err:
        logging.debug(err)
        raise Exception(err)
    if a1 < 0:
        raise Exception("first value should be between 0 and 999999")
    if a2 > 999999:
        raise Exception("second value should be between 0 and 999999")
    if a1 > a2:
        logging.debug("a1:{0} a2:{1}".format(a1, a2))
        raise Exception("first value should be less or equal than second")

    return [a1, a2]


def lab44(argv):
    a1, a2 = validate(argv)
    acc = 0
    variants = []
    for i in range(a1, a2 + 1):
        a, b, c, d, e, f = [int(x) for x in str(i).zfill(6)]
        if a + b + c == d + e + f:
            acc += 1
            variants.append("{}{}{}{}{}{}".format(a,b,c,d,e,f))

    logging.debug(str(argv) + "=" + str(acc) + " of " + str(variants))

    return acc


if __name__ == '__main__':
    lab44(sys.argv[1:])
