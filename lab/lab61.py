import sys, logging as log

console_mode = False
dicti = [1, 0, 0, 0, 1, 0, 1, 0, 2, 1]


def count(decimal):
    return sum([dicti[int(i)] for i in str(max(decimal, -decimal))])


def count_holes(param):
    try:
        a = int(param)
    except ValueError:
        console("lab61.count_holes() calculates how many holes in arabic numbers")
        console("usage: python lab61.py <decimal>")
        return "ERROR"

    holes = count(a)

    console("input = %s" % a)
    console("result = %s" % holes)

    return holes


def console(output):
    if console_mode:
        print(output)
    log.debug(output)


if __name__ == '__main__':
    console_mode = True
    count_holes(sys.argv[1])
    pass
