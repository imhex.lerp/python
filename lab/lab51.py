from sys import argv


class Comparable:
    def __init__(self, value):
        self.value = value

    def __hash__(self):
        return hash(self.value)

    def __eq__(self, other):
        if isinstance(other, self.__class__) and isinstance(other.value, self.value.__class__):
            return self.value == other.value
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

def clean_list(list_to_clean):
    return list(v.value for v in dict((Comparable(v),v) for v in list_to_clean).keys())

if __name__ == '__main__':
    clean_list(argv[1:])
