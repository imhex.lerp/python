import logging as log
import sys

console_mode = False


def console(output):
    if console_mode:
        print(output)
    log.debug(output)


def saddle_point(matrix):
    row_number = -1

    transposed = [list(i) for i in zip(*matrix)]
    for row in matrix:
        row_number += 1

        minimum = reduce(row, lambda x: min(x))
        if minimum is False:
            continue

        index_column = row.index(minimum)
        console("minimum[" + str(row_number) + "]=" + str(minimum))

        column = transposed[index_column]
        maximum = reduce(column, lambda x: max(x))
        console("maximum[" + str(index_column) + "]=" + str(maximum))

        if minimum == maximum:
            index_maximum = column.index(maximum)
            console("saddle point is: ("+str(index_maximum)+","+str(index_column)+")")
            return index_maximum, index_column

    return False


def reduce(arr, reducer):
    reduced = reducer(arr)
    index = arr.index(reduced)

    try:
        arr.index(reduced, index + 1)
    except ValueError as value_not_duplicated:
        return reduced

    return False


if __name__ == '__main__':
    console_mode = True
    saddle_point(sys.argv[1])
    pass
