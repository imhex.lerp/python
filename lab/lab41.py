import sys

import logging


def lab41(param):
    input = str.replace(param, " ", "")

    mid = int(len(input) / 2)

    first = str(input[::-1][mid:])
    second = str(input[mid:])

    logging.debug("first:%s second:%s", first, second)

    if first.lower() == second.lower():
        return "YES"
    else:
        return "NO"


if __name__ == '__main__':
    lab41(sys.argv[1:])
